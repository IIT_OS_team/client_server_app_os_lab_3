﻿#include <stdio.h>
#include <winsock2.h>
#include <vector>
#include <string>
#include <sstream>
#include <thread>
#include <iterator>
#include <iostream>
#include <fstream>
#include <mutex>

#pragma warning( disable : 4996)
#pragma comment(lib, "ws2_32.lib") //Winsock Library
using namespace std;

std::mutex g_lock;

void winsockInitialization() {

}


int main(int argc, char* argv[])
{
	vector<string> buff;
	ofstream log;
	int a = 0;
	log.open("log.txt");
	int acm1 = 0;

	thread t1([&acm1, &log, &a, &buff] {
		while (1) {
			Sleep(1000);
			g_lock.lock();
			stringstream ss;
			ss << "[" << this_thread::get_id() << "]:idle\n";
			buff.push_back(ss.str());
			log << "[" << this_thread::get_id() << "]:idle/n" << endl << flush;
			log.flush();
			g_lock.unlock();
			a+=10;
			cout << "\n" << a;
		}
	});

	WSADATA wsa;
	SOCKET master, new_socket, s;
	SOCKET set[5];
	struct sockaddr_in server, address;
	int activity, addrlen, i, valread;
	char* message = (char*) "Hello World\n";
	char* addrArr[5];
	int portArr[5];

	int MAXRECV = 10; //size of our receive buffer, this is string length.;
	//buffer = (char*)malloc((MAXRECV + 1) * sizeof(char)); //1 extra for null character, string termination
	fd_set readfds, read_master;//set of socket descriptors

	log << "Initialising Winsock... " << endl;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		log << "Failed.Error Code : " << WSAGetLastError() << endl;
		exit(EXIT_FAILURE);
	}

	log << "Initialised.\n ";
	//Create a socket
	if ((master = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		log << " Could not create socket : " << WSAGetLastError() << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < 5; i++) {
		set[i] = 0;
		addrArr[i] = 0;
		portArr[i] = 0;
	}
	log << " Socket created.\n ";

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(5223);

	//Bind
	if (bind(master, (struct sockaddr*) & server, sizeof(server)) == SOCKET_ERROR)
	{
		log << " Bind failed with error code : " <<  WSAGetLastError() << endl;
		exit(EXIT_FAILURE);
	}

	log << " Bind done " << endl;

	//Listen to incoming connections
	listen(master, 30);
	TIMEVAL timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 300;
	//Accept and incoming connection
	log << " Waiting for incoming connections... " << endl;

	addrlen = sizeof(struct sockaddr_in);
	FD_ZERO(&read_master);
	FD_ZERO(&readfds);
	int m;
	int aliveToggle = 50000;
	while (TRUE)
	{
		FD_SET(master, &read_master);
		if (SOCKET_ERROR == (m = select(0, &read_master, NULL, NULL, &timeout))) {
			log << "Socket error while selecting" << WSAGetLastError();
		}
		if (m > 0) {
			if ((new_socket = accept(master, (struct sockaddr*) & address, (int*)& addrlen)) < 0)
			{
				log << "Accept error"<< endl;
				exit(EXIT_FAILURE);
			}
			u_long nNoBlock = 1;
			ioctlsocket(new_socket, FIONBIO, &nNoBlock);
			//setsockopt(new_socket, SOL_SOCKET, SO_KEEPALIVE, (char*)& aliveToggle, sizeof(aliveToggle));
			//int x = stoi(s1);
			//inform user of socket number - used in send and receive commands
			g_lock.lock();
			stringstream ss;
			ss << "[" << this_thread::get_id() << "]:" << "accept new client " << inet_ntoa(address.sin_addr) << "\n";
			buff.push_back(ss.str());
			g_lock.unlock();
			//printf(" New connection, socket fd is % d, ip is : % s, port : % d \n ", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));

			for (int j = 0; j < 5; j++) {
				if (addrArr[j] == 0 && portArr[j] == 0) {
					addrArr[j] = inet_ntoa(address.sin_addr);
					portArr[j] = ntohs(address.sin_port);
					break;
				}
			}
			//send new connection greeting message
			if (send(new_socket, message, strlen(message), 0) != strlen(message))
			{
				log << "Send error" << "\n";
			}

			log << "Welcome message sent successfully ";
			for (int i = 0; i < 5; i++) {
				if (set[i] == 0) {
					set[i] = new_socket;
					break;
				}
			}
		}
		for (int i = 0; i < 5; i++) {
			if (set[i] != 0) {
				new_socket = set[i];
				FD_SET(new_socket, &readfds);
				getpeername(new_socket, (struct sockaddr*)& address, (int*)& addrlen);
				char* buffer;
				buffer = (char*)malloc((MAXRECV + 1) * sizeof(char));
				if (SOCKET_ERROR == (activity = select(0, &readfds, NULL, NULL, &timeout))) {
					log << ("Activity error\n");
				}
				if (activity > 0) {
					int val = recv(new_socket, buffer, MAXRECV, 0);
					if (val > 0) {
						printf(" % s: % d - %s \n ", addrArr[i], portArr[i], buffer);
						g_lock.lock();
						stringstream ss;
						ss << inet_ntoa(address.sin_addr) << " : " << ntohs(address.sin_port) << " - " << buffer << endl;
						buff.push_back(ss.str());
						g_lock.unlock();
						//printf("Client % s: % d disconnected!", addrArr[i], portArr[i]);
						//set[i] = 0;
						//addrArr[i] = 0;
						//portArr[i] = 0;
					}
					else {
						FD_CLR(new_socket, &readfds);
						set[i] = 0;
						closesocket(new_socket);
						g_lock.lock();
						stringstream ss;
						ss << "[" << this_thread::get_id() << "]: "<< "Client disconnected!" << inet_ntoa(address.sin_addr) << " : " << ntohs(address.sin_port) << "\n";
						buff.push_back(ss.str());
						g_lock.unlock();
						copy(buff.begin(), buff.end(), ostream_iterator<string>(cout << " "));
					}
				}
			}
		}
	}
	closesocket(master);
	WSACleanup();
	t1.join();
	return 0;
}